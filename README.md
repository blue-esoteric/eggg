EGGG - Eclipse, GWT, GAE, and Gradle

This is a sample app based on the article at http://www.blueesoteric.com/blog/2015/05/eggg-eclipse-gwt-gae-and-gradle. 

Requirements

Eclipse with GPE and Gradle.

Recommended is the SDBG Plugin (http://sdbg.github.io/)